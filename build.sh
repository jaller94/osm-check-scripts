#!/bin/sh

set -e

bun run src/downloaders/7-eleven.ts
bun run src/downloaders/pxmart.ts
#bun run src/downloaders/watsons.ts
bun run src/downloaders/stolpersteine-berlin.ts
bun run src/download.ts

bun run src/index.ts | pino-pretty

cd hugo
HUGO_ENVIRONMENT=production hugo --cleanDestinationDir --minify
rsync -avzP --delete ./public/ chrpaul.de:~/website/osm-check.chrpaul.de/
cd -
