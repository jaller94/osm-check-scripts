import fs from 'node:fs';
import path from 'node:path';
import pino from 'pino';

const PATH_CONFIG = process.env.PATH_CONFIG || './config';

let options;
let fileExists = true;
try {
    const data = fs.readFileSync(path.join(PATH_CONFIG, 'pino.json'), 'utf8');
    options = JSON.parse(data);
} catch (err: any) {
    if (err.code !== 'ENOENT') {
        console.error(err);
        process.exit(1);
    }
    fileExists = false;
}

export const logger = pino(options);
