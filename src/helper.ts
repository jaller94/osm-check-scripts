/**
 * This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in meters)
 */
export const calcCrow = (lat1: number, lon1: number, lat2: number, lon2: number): number => {
    var R = 6371; // km
    var dLat = toRad(lat2-lat1);
    var dLon = toRad(lon2-lon1);
    var lat1 = toRad(lat1);
    var lat2 = toRad(lat2);

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c;
    return d;
};

// Converts numeric degrees to radians
const toRad = (deg: number): number => {
    return deg * Math.PI / 180;
};

export function groupBy<T>(array: T[], getKey: (item: T) => string | undefined): Map<string | undefined, T[]> {
    const map = new Map<string | undefined, T[]>();
    for (const item of array) {
        const key = getKey(item);
        map.set(key, [
            ...(map.get(key) ?? []),
            item,
        ]);
    }
    return map;
}

export const getOpenStreetMapCopyright = () => ({
    author: 'OpenStreetMap contributors',
    licenseUri: 'https://opendatacommons.org/licenses/odbl/',
    year: new Date().getFullYear(),
});

export function esc<T extends string | unknown>(str: T): T {
    if (typeof str !== 'string') {
        return str;
    }
    return str.replace(/([\!\*\\\[\-\~\]])/g, '\\$1') as T;
}
