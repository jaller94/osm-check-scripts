import { create } from "xmlbuilder2";
import { calcCrow } from "./helper";
import { NewOsmElement, OsmElement } from "./types";

export function getSinglePositionFromGeoJsonGeometry(geometry: any): [number, number] {
    let a = geometry.coordinates;
    if (typeof a[0] === 'number') {
        return a;
    }
    a = a[0];
    if (typeof a[0] === 'number') {
        return a;
    }
    a = a[0];
    if (typeof a[0] === 'number') {
        return a;
    }
    a = a[0];
    if (typeof a[0] === 'number') {
        return a;
    }
    console.warn(geometry);
    throw Error('No position found');
}

export function getSinglePositionFromTurboExpressElement(element: OsmElement | NewOsmElement): [number, number] {
    try {
        if (element.type === 'node') {
            return [ element.lat, element.lon ];
        } else if (element.type === 'way') {
            return [ element.geometry[0].lat, element.geometry[0].lon ];
        } else {
            return getSinglePositionFromTurboExpressElement(element.members[0]);
        }
    } catch (err) {
        console.error(element);
        throw err;
    }
}

export function findNearbyElements<T extends OsmElement | NewOsmElement>(lat: number, lon: number, elements: T[], maxDistance = 40): {
    distance: number,
    element: T,
}[] {
    const arr: {
        distance: number,
        element: T,
    }[] = [];
    for (const element of elements) {
        try {
            const distance = calcCrow(
                lat, lon,
                ...getSinglePositionFromTurboExpressElement(element)
            ) * 1000;
            if (distance < maxDistance) {
                arr.push({
                    distance,
                    element,
                });
            }
        } catch (err) {
            console.error(element);
            throw err;
        }
    }
    arr.sort((a, b) => a.distance - b.distance);
    return arr;
}

type GpxOptions = {
    name?: string,
    description?: string,
    copyright?: {
        author: string,
        year?: number,
        licenseUri?: string,
    }
};

export const osmElementToGpx = <T extends OsmElement | NewOsmElement>(
    elements: T[],
    describeElement: (element: T) => string, opts?: GpxOptions
): string => {
    const doc = create({ version: '1.0' });
    const gpx = doc.ele('gpx', {
        xmlns: 'http://www.topografix.com/GPX/1/1',
        version: '1.1',
        'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemaLocation': 'http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd'
    });
    const metadata = gpx.ele('metadata');
    if (opts?.name) {
        metadata.ele('name').txt(opts.name);
    }
    if (opts?.description) {
        metadata.ele('desc').txt(opts.description);
    }
    if (opts?.copyright) {
        const copyright = metadata.ele('copyright', {
            author: opts.copyright.author,
        });
        if (opts.copyright.year) {
            metadata.ele('year').txt(opts.copyright.year.toFixed(0));
        }
        if (opts.copyright.licenseUri) {
            metadata.ele('license').txt(opts.copyright.licenseUri);
        }
    }
    for (const element of elements) {
        const pos = getSinglePositionFromTurboExpressElement(element);
        const name = describeElement(element);
        gpx.ele('wpt', {
            lat: pos[0],
            lon: pos[1],
        }).ele('name').txt(name);
    }
    return doc.end({ prettyPrint: true }).replace(/\n\W+<name>/g, '<name>').replace(/<\/name>\n/g, '</name>');
};
