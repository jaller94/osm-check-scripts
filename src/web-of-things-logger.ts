import { logger } from './logger';

const log = logger.child({name: 'web-of-things-logger'});

export async function logTaskResult(taskName: string, duration: number, taskErr?: Record<string, unknown> | undefined) {
    const res = await fetch(`${process.env.TASK_MONITORS_URL}/task-monitors/${taskName}/actions/report`, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            duration,
            error: taskErr,
        }),
    });
    if (!res.ok) {
        log.error({
            status: res.status,
            statusText: res.statusText,
            taskName,
            duration,
            taskErr,
        }, 'Failed to report task completion');
    }
}
