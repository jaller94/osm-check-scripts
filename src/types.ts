export type NewOsmElement = {
    tags: Record<string, string | undefined>,
} & ({
    type: 'node',
    lat: number,
    lon: number,
} | {
    type: 'way',
    geometry: {lat: number, lon: number}[],
} | {
    type: 'relation',
    members: OsmElement[],
});

export type OsmElement = {
    id: number,
} & NewOsmElement;

export type StolpersteinBerlin = {
    first_name: string,
    last_name: string | null,
    street: string,
    house_no: string | null,
    geo__lng: string,
    geo__lat: string,
    url: string,
    lat: number,
    lon: number,
    name: string,
};
