import fs from 'node:fs/promises';
import { logger } from '../logger';
import { NewOsmElement, StolpersteinBerlin } from "../types";
import z from 'zod';

const log = logger.child({name: 'downloader/stolpersteine-berlin'});

export const stolpersteinType = z.object({
    district: z.nullable(z.string()),
    district_id: z.nullable(z.string()),
    first_name: z.string(),
    geo__lat: z.string(),
    geo__lng: z.string(),
    house_no: z.nullable(z.string()),
    last_name: z.nullable(z.string()),
    localdistrict: z.string(),
    localdistrict_id: z.string(),
    street: z.string(),
    url: z.string(),
    virtual: z.enum(['0', 'virtuell']),
});

export function cleanNode(stolperstein: unknown) {
    const parsed = stolpersteinType.safeParse(stolperstein);
    if (!parsed.success) {
        log.error({err: parsed.error.errors}, 'Stolperstein data error');
        throw Error('Stolperstein data error');
    }
    const stolpersteinChecked = parsed.data;
    // Remove alternative names. These are written like "Becker [Beker]".
    let name = stolpersteinChecked.first_name.replace(/ \[[^\\]*\]/, '').trim().replace(/ {2,}/, ' ');
    if (stolpersteinChecked.last_name) {
        name += ` ${stolpersteinChecked.last_name.replace(/ \[[^\\]*\]/, '').trim().replace(/ {2,}/, ' ')}`;
    }
    return {
        ...stolpersteinChecked,
        lat: Number.parseFloat(stolpersteinChecked.geo__lat),
        lon: Number.parseFloat(stolpersteinChecked.geo__lng),
        name,
    };
}

export function convertToNode(stolperstein: StolpersteinBerlin): NewOsmElement {
    return {
        type: 'node',
        lat: stolperstein.lat,
        lon: stolperstein.lon,
        tags: {
            name: stolperstein.name,
            'memorial:addr:street': stolperstein.street.replace('str.', 'straße').replace('Str.', 'Straße').trim().replace(/ {2,}/, ' '),
            ...(stolperstein.house_no && {
                'memorial:addr:housenumber': stolperstein.house_no.trim().replace(/ {2,}/, ' ')
            }),
            historical: 'memorial',
            memorial: 'stolperstein',
            website: stolperstein.url.trim(),
        },
    };
}

async function main() {
    const res = await fetch('https://www.stolpersteine-berlin.de/en/api/json/stolpersteine.json');
    if (!res.ok) {
        throw Error('Download failed');
    }
    const data = await res.text();
    await fs.writeFile(`data/3rd-party/stolpersteine-berlin.json`, data, 'utf8');
    await fs.writeFile(`data/3rd-party/stolpersteine-berlin-osm.json`, JSON.stringify(JSON.parse(data).data.map(cleanNode).map(convertToNode), undefined, 2), 'utf8');
}

main();
