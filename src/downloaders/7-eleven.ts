import fs from 'node:fs/promises';
import querystring from 'node:querystring';
import { create } from 'xmlbuilder2';
import { NewOsmElement } from '../types';
import { logger } from '../logger';

const log = logger.child({name: 'downloader/7-eleven'});

function convertXmlToOject(xml: string) {
    const doc = create(xml);
    const obj = doc.end({ format: 'object' });
    return obj;
}

function convertToNode(store: Record<string, unknown>): NewOsmElement {
    if (typeof store.X !== 'string') {
        throw Error('X needs to be a string');
    }
    if (typeof store.Y !== 'string') {
        throw Error('Y needs to be a string');
    }
    if (typeof store.POIID !== 'string') {
        throw Error('POIID needs to be a string');
    }
    if (typeof store.POIName !== 'string') {
        throw Error('POIName needs to be a string');
    }
    const features: string = typeof store.StoreImageTitle === 'string' ? store.StoreImageTitle : '';
    return {
        type: 'node',
        lat: Number.parseFloat(`${store.Y.slice(0, 2)}.${store.Y.slice(2)}`),
        lon: Number.parseFloat(`${store.X.slice(0, 3)}.${store.X.slice(3)}`),
        tags: {
            ref: store.POIID.trim(),
            branch: store.POIName.trim(),
            'addr:full': store.Address,
            ...(features.includes('02廁所') && {
                toilets: 'yes',
                'toilets:access': 'customers',
            }),
            ...(features.includes('03ATM') && {atm: 'yes'}),
            ...(features.includes('04座位區') && {bench: 'yes'}),
            ...(features.includes('05ibon WiFi') && {internet_access: 'yes'}),
            ...(features.includes('19ibon') && {
                'service:scan': 'yes',
                'service:print': 'yes',
            }),
        },
    };
}

async function searchStore(city: string, town: string): Promise<string> {
    const res = await fetch('https://emap.pcsc.com.tw/EMapSDK.aspx', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        body: querystring.stringify({
            commandid : 'SearchStore',
            city,
            town,
            roadname: '',
            ID: '',
            StoreName: '',
            SpecialStore_Kind: '',
            leftMenuChecked: '',
            address: '',
        }),
    });

    if (!res.ok) {
        log.error(res);
        throw Error(`HTTP Error ${res.status} (${res.statusText})`);
    }
    const text = await res.text();
    if (text === '') {
        throw Error('Empty response');
    }
    return text;
}

const tw = {
    /* orange */
    '基隆市': '中正區,七堵區,暖暖區,安樂區,中山區,仁愛區,信義區'.split(','),
    // Taipei
    '台北市': '松山區,信義區,大安區,中山區,中正區,大同區,萬華區,文山區,南港區,內湖區,士林區,北投區'.split(','),
    '新北市': '板橋區,三重區,中和區,永和區,新莊區,新店區,樹林區,土城區,蘆洲區,汐止區,鶯歌區,三峽區,淡水區,瑞芳區,五股區,泰山區,林口區,深坑區,石碇區,坪林區,三芝區,石門區,八里區,平溪區,雙溪區,貢寮區,金山區,萬里區,烏來區'.split(','),

    /* yellow */
    '桃園市': '桃園區,中壢區,八德區,平鎮區,龜山區,大溪區,楊梅區,蘆竹區,大園區,龍潭區,新屋區,觀音區,復興區'.split(','),
    '新竹市': '東區,北區,香山區'.split(','),
    '新竹縣': '竹北市,竹東鎮,新埔鎮,關西鎮,湖口鄉,新豐鄉,芎林鄉,橫山鄉,北埔鄉,寶山鄉,峨眉鄉,尖石鄉,五峰鄉'.split(','),
    '苗栗縣': '苗栗市,竹南鎮,後龍鎮,頭份市,造橋鄉,三灣鄉,頭屋鄉,獅潭鄉,南庄鄉,公館鄉,西湖鄉,通霄鎮,苑裡鎮,銅鑼鄉,大湖鄉,三義鄉,卓蘭鎮,泰安鄉'.split(','),
    
    /* blue */
    '台中市': '豐原區,中區,大里區,東區,南區,太平區,和平區,西區,北區,東勢區,后里區,西屯區,外埔區,南屯區,北屯區,大甲區,大安區,清水區,神岡區,石岡區,新社區,梧棲區,沙鹿區,大雅區,潭子區,龍井區,大肚區,烏日區,霧峰區'.split(','),
    '彰化縣': '彰化市,鹿港鎮,伸港鄉,線西鄉,秀水鄉,花壇鄉,芬園鄉,福興鄉,埔鹽鄉,大村鄉,員林市,埔心鄉,溪湖鎮,芳苑鄉,二林鎮,埤頭鄉,田尾鄉,永靖鄉,社頭鄉,田中鎮,北斗鎮,二水鄉,溪州鄉,竹塘鄉,大城鄉,和美鎮'.split(','),
    '南投縣': '南投市,埔里鎮,草屯鎮,竹山鎮,集集鎮,名間鄉,鹿谷鄉,中寮鄉,魚池鄉,國姓鄉,水里鄉,信義鄉,仁愛鄉'.split(','),

    /* brown */
    '雲林縣': '斗六市,斗南鎮,虎尾鎮,西螺鎮,土庫鎮,北港鎮,口湖鄉,麥寮鄉,崙背鄉,二崙鄉,莿桐鄉,林內鄉,古坑鄉,大埤鄉,褒忠鄉,元長鄉,東勢鄉,台西鄉,四湖鄉,水林鄉'.split(','),
    '嘉義縣': '太保市,朴子市,布袋鎮,大林鎮,民雄鄉,溪口鄉,新港鄉,六腳鄉,東石鄉,義竹鄉,鹿草鄉,水上鄉,中埔鄉,竹崎鄉,梅山鄉,番路鄉,大埔鄉,阿里山鄉'.split(','),
    '嘉義市': '東區,西區'.split(','),
    
    /* green */
    // Tainan
    '台南市': '新營區,安南區,安平區,永康區,北區,白河區,中西區,鹽水區,南區,柳營區,東區,後壁區,東山區,北門區,學甲區,下營區,六甲區,楠西區,將軍區,佳里區,麻豆區,官田區,大內區,玉井區,南化區,七股區,西港區,安定區,善化區,新市區,山上區,新化區,左鎮區,龍崎區,關廟區,歸仁區,仁德區'.split(','),
    // Kaohisung
    '高雄市': '鳳山區,鹽埕區,鼓山區,林園區,大寮區,左營區,大樹區,楠梓區,大社區,三民區,仁武區,新興區,前金區,鳥松區,岡山區,苓雅區,前鎮區,橋頭區,燕巢區,旗津區,田寮區,小港區,阿蓮區,路竹區,湖內區,茄萣區,永安區,彌陀區,梓官區,旗山區,美濃區,六龜區,甲仙區,杉林區,內門區,茂林區,桃源區,那瑪夏區'.split(','),
    // Pingtung County
    '屏東縣': '屏東市,潮州鎮,東港鎮,恆春鎮,萬丹鄉,長治鄉,麟洛鄉,九如鄉,里港鄉,鹽埔鄉,高樹鄉,萬巒鄉,內埔鄉,竹田鄉,新埤鄉,枋寮鄉,新園鄉,崁頂鄉,林邊鄉,南州鄉,佳冬鄉,琉球鄉,車城鄉,滿州鄉,枋山鄉,三地門鄉,霧台鄉,瑪家鄉,泰武鄉,來義鄉,春日鄉,獅子鄉,牡丹鄉'.split(','),
    
    /* purple */
    '宜蘭縣': '宜蘭市,羅東鎮,蘇澳鎮,頭城鎮,礁溪鄉,壯圍鄉,員山鄉,冬山鄉,五結鄉,三星鄉,大同鄉,南澳鄉'.split(','),
    '花蓮縣': '花蓮市,鳳林鎮,玉里鎮,新城鄉,吉安鄉,壽豐鄉,光復鄉,豐濱鄉,瑞穗鄉,富里鄉,秀林鄉,萬榮鄉,卓溪鄉'.split(','),
    '台東縣': '台東市,成功鎮,關山鎮,卑南鄉,鹿野鄉,池上鄉,東河鄉,長濱鄉,太麻里鄉,大武鄉,綠島鄉,海端鄉,延平鄉,金峰鄉,達仁鄉,蘭嶼鄉'.split(','),

    /* islands */
    '連江縣': '南竿鄉,北竿鄉,莒光鄉,東引鄉'.split(','),
    '金門縣': '金湖鎮,金沙鎮,金城鎮,金寧鄉,烈嶼鄉'.split(','),
    '澎湖縣': '馬公市,湖西鄉,白沙鄉,西嶼鄉,望安鄉,七美鄉'.split(','),
};

async function main(download = true) {
    const allStores: unknown[] = [];
    for (const [city, towns] of Object.entries(tw)) {
        for (const town of towns) {
            try {
                let xml: string;
                if (download) {
                    xml = await searchStore(city, town);
                    await fs.writeFile(`data/7-eleven/${city}-${town}.xml`, xml, 'utf8');
                } else {
                    xml = await fs.readFile(`data/7-eleven/${city}-${town}.xml`, 'utf8');
                }
                const xmlObject = convertXmlToOject(xml);
                let stores = xmlObject.iMapSDKOutput.GeoPosition;
                if (!Array.isArray(stores)) {
                    stores = [stores];
                }
                try {
                    const osmNodes = stores.map(convertToNode);
                    await fs.writeFile(`data/7-eleven-osm/${city}-${town}.json`, JSON.stringify(osmNodes, undefined, 2), 'utf8');
                    allStores.push(...stores);
                } catch (err) {
                    log.error({ err, xmlObject });
                    throw err;
                }
            } catch (err) {
                log.error({ err, city, town });
            }
        }
    }
    await fs.writeFile(`data/7-eleven.json`, JSON.stringify(allStores, undefined, 2), 'utf8');
    await fs.writeFile(`data/7-eleven-osm.json`, JSON.stringify(allStores.map(convertToNode), undefined, 2), 'utf8');
}

main();
