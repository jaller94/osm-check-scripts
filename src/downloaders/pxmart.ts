import fs from 'node:fs/promises';
import Ajv from 'ajv';
import schema from '../schemas/pxmart.json';
import { NewOsmElement } from '../types';

const ajv = new Ajv({
    strict: false,
});
const validate = ajv.compile(schema);

function convertToNode(store: Record<string, unknown>): NewOsmElement {
    if (typeof store.id !== 'number') {
        throw Error('id needs to be a number');
    }
    if (typeof store.attributes !== 'object' || !store.attributes) {
        throw Error('attributes needs to be an object');
    }
    if (!('latitude' in store.attributes) || typeof store.attributes.latitude !== 'number') {
        throw Error('attributes.latitude needs to be a number');
    }
    if (!('longitude' in store.attributes) || typeof store.attributes.longitude !== 'number') {
        throw Error('attributes.longitude needs to be a number');
    }
    return {
        type: 'node',
        lat: store.attributes.latitude,
        lon: store.attributes.longitude,
        tags: {
            ref: store.id.toString(),
        },
    };
}

async function searchStore(): Promise<string> {
    const res = await fetch('https://www.pxmart.com.tw/api/stores', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body:JSON.stringify({
            area: '',
            city: '',
            name: '',
            services: [],
            street: '',
        }),
    });
    if (!res.ok) {
        console.error(res);
        throw Error(`HTTP Error ${res.status} (${res.statusText})`);
    }
    console.log(res.status, res.statusText);
    const text = await res.text();
    if (text === '') {
        throw Error('Empty response');
    }
    return text;
}

async function main(download = true) {
    const allStores: unknown[] = [];
    try {
        const json = await searchStore();
        await fs.writeFile(`data/pxmart.json`, json, 'utf8');
        const data = JSON.parse(json);

        const valid = validate(data);
        if (!valid) {
            console.log(validate.errors);
            throw Error('Validation failed');
        }

        allStores.push(...data.data);
    } catch (err) {
        console.error(err);
    
    }
    await fs.writeFile(`data/pxmart.json`, JSON.stringify(allStores, undefined, 2), 'utf8');
    await fs.writeFile(`data/pxmart-osm.json`, JSON.stringify(allStores.map(convertToNode), undefined, 2), 'utf8');
}

main();
