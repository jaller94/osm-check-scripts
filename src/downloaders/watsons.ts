import fs from 'node:fs/promises';
import { URLSearchParams } from 'node:url';
import Ajv from 'ajv';
import schema from '../schemas/watsons.json';
import { NewOsmElement } from '../types';

const ajv = new Ajv({
    strict: false,
});
const validate = ajv.compile(schema);

function convertToNode(store: Record<string, unknown>): NewOsmElement {
    if (typeof store.elabStoreNumber !== 'string') {
        throw Error('elabStoreNumber needs to be a string');
    }
    if (typeof store.displayName !== 'string') {
        throw Error('displayName needs to be a string');
    }
    if (typeof store.geoPoint !== 'object' || !store.geoPoint) {
        throw Error('geoPoint needs to be an object');
    }
    if (!('latitude' in store.geoPoint) || typeof store.geoPoint.latitude !== 'number') {
        throw Error('geoPoint.latitude needs to be a number');
    }
    if (!('longitude' in store.geoPoint) || typeof store.geoPoint.longitude !== 'number') {
        throw Error('geoPoint.longitude needs to be a number');
    }
    return {
        type: 'node',
        lat: store.geoPoint.latitude,
        lon: store.geoPoint.longitude,
        tags: {
            branch: store.displayName,
            ref: store.elabStoreNumber.trim(),
        },
    };
}

async function searchStore(currentPage: number): Promise<string> {
    const url = new URL('https://api.watsons.com.tw/api/v2/wtctw/stores/watStores');
    url.search = new URLSearchParams({
        currentPage: currentPage.toString(),
        pageSize: '20',
        isCceOrCc: 'false',
        fields: 'FULL',
        lang: 'zh_TW',
        curr: 'TWD',
    }).toString();
    const res = await fetch(url, {
        headers: {
            Accept: 'application/json, text/plain, */*',
            Authorization: 'bearer lvfIPQutllSmUr09W37d5dbV3uM',
            Origin: 'https://www.watsons.com.tw',
            Referer: 'https://www.watsons.com.tw/',
            'queue-target': 'https://www.watsons.com.tw/store-finder',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:130.0) Gecko/20100101 Firefox/130.0',
        },
    });
    if (!res.ok) {
        console.error(res);
        throw Error(`HTTP Error ${res.status} (${res.statusText})`);
    }
    console.log(res.status, res.statusText);
    const text = await res.text();
    if (text === '') {
        throw Error('Empty response');
    }
    return text;
}

async function main(download = true) {
    const allStores: unknown[] = [];
    let page = 0;
    let totalPages = 1;
    while (page < totalPages) {
        console.log(`watson downloader: downloading page ${page}`);
        let json: string;
        if (download) {
            json = await searchStore(page);
            await fs.writeFile(`data/watsons/${page}.json`, json, 'utf8');
        } else {
            json = await fs.readFile(`data/watsons/${page}.json`, 'utf8');
        }
        
        const data = JSON.parse(json);

        const valid = validate(data);
        if (!valid) {
            console.log(validate.errors);
            throw Error('Validation failed');
        }

        totalPages = data.pagination.totalPages;
        allStores.push(...data.stores);
        console.log(`watson downloader: got page ${page} of ${totalPages}, currently ${allStores.length} stores`)
        page++;
    }
    await fs.writeFile(`data/watsons.json`, JSON.stringify(allStores, undefined, 2), 'utf8');
    await fs.writeFile(`data/watsons-osm.json`, JSON.stringify(allStores.map(convertToNode), undefined, 2), 'utf8');
}

main();
