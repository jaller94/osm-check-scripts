import fs from 'node:fs/promises';
import path from 'node:path';
import { logger } from './logger';
import * as groceryStores from './grocery-stores';
import * as metro from './metro';
import * as youbikes from './youbikes';
import { stolpersteineBerlinComplete, stolpersteineBerlinCompleteByLocalDistrict, stolpersteineBerlinSummary } from './stolpersteine';
import { logTaskResult } from './web-of-things-logger';

const log = logger.child({name: 'index'});

const jsonFiles: Map<string, any> = new Map();

const loadJsonFile = async (filePath: string): Promise<any> => {
    try {
        let data = jsonFiles.get(filePath);
        if (data === undefined) {
            data = JSON.parse(await fs.readFile(filePath, 'utf8'));
            jsonFiles.set(filePath, data);
        }
        return data;
    } catch (err) {
        log.error({err}, `Failed to load ${filePath}`);
    }
};

const patchPage = async (filePath: string, text: string) => {
    let input = await fs.readFile(filePath, 'utf8');
    input = input.replace(/<!--START-->.*?<!--END-->/gs, `<!--START-->\n${text}\n<!--END-->`);
    await fs.writeFile(filePath, input, 'utf8');
}

const patchPageHeader = async (filePath: string, key: string, value: string) => {
    let input = await fs.readFile(filePath, 'utf8');
    input = input.replace(new RegExp(`^${key}: .+$`, 'm'), `${key}: ${value}`);
    await fs.writeFile(filePath, input, 'utf8');
}

const patchLastmod = async (filePath: string) => {
    await patchPageHeader(filePath, 'lastmod', new Date().toISOString());
}

async function executeTask(slug: string, func: (slug: string) => Promise<void>): Promise<void> {
    const startTime = new Date().getTime();
    try {
        await func(slug);
        const duration = new Date().getTime() - startTime;
        log.info({task: slug, duration}, `Processed in ${duration} ms.`);
        void logTaskResult(slug, duration);
    } catch (err: unknown) {
        const duration = new Date().getTime() - startTime;
        log.error({task: slug, duration, err});
        if (err instanceof Error) {
            void logTaskResult(slug, duration, {
                name: err.name,
                message: err.message,
                stack: err.stack,
            });
        }
    }
}

const main = async () => {
    const officialYouBikeStations = [
        ...await loadJsonFile('data/station-yb2.json'),
    ];
    const osmGroceryStores = [
        ...(await loadJsonFile('data/chemist.json')).elements,
        ...(await loadJsonFile('data/convenience.json')).elements,
        ...(await loadJsonFile('data/supermarkets.json')).elements,
    ];
    const sevenElevenStores = await loadJsonFile('data/7-eleven-osm.json');
    const pxmartStores = await loadJsonFile('data/pxmart-osm.json');
    const watsonsStores = await loadJsonFile('data/watsons-osm.json');
    const osmYouBikesData = await loadJsonFile('data/youbikes.json');
    const osmYouBikes = osmYouBikesData.elements;
    const osmMetroData = await loadJsonFile('data/metro.json');
    const osmMetro = osmMetroData.elements;
    // // console.dir(osmYouBikes.map(station => station.properties.ref).join(','));
    // // const gpx = stationsToGpx(officialYouBikeStations);
    // // await fs.writeFile('stations.gpx', gpx, 'utf8');

    const officialStolpersteineBerlin = await loadJsonFile('data/3rd-party/stolpersteine-berlin.json');
    const officialStolpersteineBerlinOsm = await loadJsonFile('data/3rd-party/stolpersteine-berlin-osm.json');
    const osmStolpersteineBerlin = (await loadJsonFile('data/stolpersteine-berlin.json')).elements;

    if (!process.env.OSM_CHECK_WEBSITE) {
        throw Error('Need OSM_CHECK_WEBSITE');
    }

    const OSM_CHECK_WEBSITE = process.env.OSM_CHECK_WEBSITE;

    await executeTask('youbikes-duplicate-refs', async (slug: string) => {
        const a = youbikes.duplicateRefs(osmYouBikes);
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-duplicate-refs.md'), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-duplicate-refs.md'));
    });
    await executeTask('youbikes-missing-ref', async () => {
        const a = youbikes.missingRef(osmYouBikes);
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-missing-ref.md'), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-missing-ref.md'));
    });
    await executeTask('youbikes-too-close', async () => {
        const a = youbikes.duplicatePosition(osmYouBikes);
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-too-close.md'), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-too-close.md'));
    });
    await executeTask('youbikes-complete', async () => {
        const a = youbikes.completeRefs(officialYouBikeStations, osmYouBikes);
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-complete/index.md'), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-complete/index.md'));
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-complete/index.zh-tw.md'), a.textZh);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-complete/index.zh-tw.md'));
        for (const [name, body] of Object.entries(a.files)) {
            await fs.writeFile(path.join(OSM_CHECK_WEBSITE, `content/report/youbikes-complete`, name), body);
        }
    });
    await executeTask('youbikes-no-surplus', async () => {
        const a = youbikes.noSurplus(officialYouBikeStations, osmYouBikes);
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-no-surplus/index.md'), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-no-surplus/index.md'));
    });
    await executeTask('youbikes-complete-tags', async () => {
        const a = youbikes.completeTags(osmYouBikes);
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-complete-tags.md'), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-complete-tags.md'));
    });
    await executeTask('youbikes-correct-tags', async () => {
        const a = youbikes.correctTags(osmYouBikes);
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-correct-tags.md'), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-correct-tags.md'));
    });
    await executeTask('youbikes-different-position', async () => {
        const a = youbikes.differentPosition(officialYouBikeStations, osmYouBikes);
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-different-position.md'), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/youbikes-different-position.md'));
    });
    await executeTask('taiwan-metro-entrances-wheelchair', async () => {
        const a = metro.entranceAccessibilityMissing(osmMetro);
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/taiwan-metro-entrances-wheelchair/index.md'), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/taiwan-metro-entrances-wheelchair/index.md'));
        for (const [name, body] of Object.entries(a.files)) {
            await fs.writeFile(path.join(OSM_CHECK_WEBSITE, 'content/report/taiwan-metro-entrances-wheelchair', name), body);
        }
    });
    await executeTask('taiwan-grocery-stores', async () => {
        const a = groceryStores.summary(osmGroceryStores);
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/taiwan-grocery-stores.md'), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/taiwan-grocery-stores.md'));
    });
    await executeTask('taiwan-grocery-stores-by-brands', async () => {
        const a = groceryStores.byBrand(osmGroceryStores);
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/taiwan-grocery-stores-by-brands.md'), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/taiwan-grocery-stores-by-brands.md'));
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/taiwan-grocery-stores-by-brands.zh-tw.md'), a.textZh);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/taiwan-grocery-stores-by-brands.zh-tw.md'));
    });
    await executeTask('taiwan-7-eleven-no-surplus', async () => {
        const slug = '7-eleven';
        const a = groceryStores.chainNoSurplus(sevenElevenStores, osmGroceryStores, slug, '7-Eleven');
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-no-surplus/index.md`), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-no-surplus/index.md`));
        for (const [name, body] of Object.entries(a.files)) {
            await fs.writeFile(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-no-surplus`, name), body);
        }
    });
    await executeTask('taiwan-7-eleven-complete', async () => {
        const slug = '7-eleven';
        const a = groceryStores.chainCompleteRefs(sevenElevenStores, osmGroceryStores, slug, '7-Eleven', ' 7-Eleven ', {
            author: 'President Chain Store Cooperation',
        });
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete/index.md`), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete/index.md`));
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete/index.zh-tw.md`), a.textZh);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete/index.zh-tw.md`));
        for (const [name, body] of Object.entries(a.files)) {
            await fs.writeFile(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete`, name), body);
        }
    });
    await executeTask('taiwan-pxmart-complete', async () => {
        const slug = 'pxmart';
        const a = groceryStores.chainCompleteRefs(pxmartStores, osmGroceryStores, slug, 'Pxmart', '全聯福利中心', {
            author: 'PX Mart Co., Ltd.',
        });
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete/index.md`), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete/index.md`));
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete/index.zh-tw.md`), a.textZh);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete/index.zh-tw.md`));
        for (const [name, body] of Object.entries(a.files)) {
            await fs.writeFile(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete`, name), body);
        }
    });
    await executeTask('taiwan-watsons-complete', async () => {
        const slug = 'watsons';
        const a = groceryStores.chainCompleteRefs(watsonsStores, osmGroceryStores, slug, 'watsons', '屈臣氏', {
            author: 'A.S. Watson Group',
        });
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete/index.md`), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete/index.md`));
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete/index.zh-tw.md`), a.textZh);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete/index.zh-tw.md`));
        for (const [name, body] of Object.entries(a.files)) {
            await fs.writeFile(path.join(OSM_CHECK_WEBSITE, `content/report/taiwan-${slug}-complete`, name), body);
        }
    });
    await executeTask('taiwan-grocery-stores-too-close', async () => {
        const a = groceryStores.duplicatesByPosition(osmGroceryStores);
        await patchPage(path.join(OSM_CHECK_WEBSITE, 'content/report/taiwan-grocery-stores-too-close/index.md'), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, 'content/report/taiwan-grocery-stores-too-close/index.md'));
        for (const [name, body] of Object.entries(a.files)) {
            await fs.writeFile(path.join(OSM_CHECK_WEBSITE, 'content/report/taiwan-grocery-stores-too-close', name), body);
        }
    });
    await executeTask('stolpersteine-berlin-complete', async () => {
        const slug = 'stolpersteine-berlin-complete';
        const a = stolpersteineBerlinComplete(officialStolpersteineBerlinOsm, osmStolpersteineBerlin);
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}/index.md`), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}/index.md`));
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}/index.de.md`), a.textDe);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}/index.de.md`));
        for (const [name, body] of Object.entries(a.files)) {
            await fs.writeFile(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}`, name), body);
        }
    });
    await executeTask('stolpersteine-berlin', async () => {
        const slug = 'stolpersteine-berlin';
        const a = stolpersteineBerlinSummary(osmStolpersteineBerlin);
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}/index.md`), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}/index.md`));
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}/index.de.md`), a.textDe);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}/index.de.md`));
    });
    await executeTask('stolpersteine-berlin-complete-by-kiez', async () => {
        const slug = 'stolpersteine-berlin-complete-by-kiez';
        const a = stolpersteineBerlinCompleteByLocalDistrict(officialStolpersteineBerlin.data, osmStolpersteineBerlin);
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}/index.md`), a.text);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}/index.md`));
        await patchPage(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}/index.de.md`), a.textDe);
        await patchLastmod(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}/index.de.md`));
        for (const [name, body] of Object.entries(a.files)) {
            await fs.writeFile(path.join(OSM_CHECK_WEBSITE, `content/report/${slug}`, name), body);
        }
    });
};

main();
