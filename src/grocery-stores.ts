import { calcCrow, esc, getOpenStreetMapCopyright, groupBy } from './helper';
import { findNearbyElements, getSinglePositionFromTurboExpressElement, osmElementToGpx } from './helper-geo';
import { NewOsmElement, OsmElement } from "./types";

export function summary(elements: OsmElement[]) {
    let text = 'An analysis of grocery stores in Taiwan.\n';
    text += `<!--more-->\n`;
    text += `## Shop types\n`;
    for (const type of ['convenience', 'supermarket']) {
        text += `  * ${elements.filter(e => e.tags['shop'] === type).length} of ${type}\n`;
    }
    text += `## OpenStreetMap types\n`;
    for (const type of ['node', 'way', 'relation']) {
        text += `  * ${elements.filter(e => e.type === type).length} of ${type}\n`;
    }
    text += `\n## Brands\n`;
    text += `### \`brand\`\n`;
    const byBrand = groupBy(elements, e =>  e.tags['brand']);
    for (const [brand, elements] of [...byBrand.entries()].sort((a, b) => b[1].length - a[1].length)) {
        text += `  * ${elements.length} of ${brand}\n`;
    }
    text += `### \`brand:en\`\n`;
    const byBrandEn = groupBy(elements, e =>  e.tags['brand:en']);
    for (const [brand, elements] of [...byBrandEn.entries()].sort((a, b) => b[1].length - a[1].length)) {
        text += `  * ${elements.length} of ${brand}\n`;
    }
    text += `### \`brand:wikidata\`\n`;
    const byBrandWikidata = groupBy(elements, e =>  e.tags['brand:wikidata']);
    for (const [brand, elements] of [...byBrandWikidata.entries()].sort((a, b) => b[1].length - a[1].length)) {
        text += `  * ${elements.length} of ${brand}\n`;
    }

    return {
        text,
    }
}

export function byBrand(elements: OsmElement[]) {
    let text = 'A list of grocery stores in Taiwan by brands.\n';
    let textZh = '';
    text += `<!--more-->\n`;
    textZh += `<!--more-->\n`;
    text += `The list is grouped by the tag \`brand:wikidata\`.\n`;
    textZh += `此清單按標籤“brand:wikidata”分組。\n`;
    const byBrand = groupBy(elements, e => e.tags['brand:wikidata']);
    for (const [brand, elements] of [...byBrand.entries()].sort((a, b) => a[1].length - b[1].length)) {
        const name = brand ? `${esc(elements[0].tags.brand ?? brand)}${!!elements[0].tags['brand:en'] && elements[0].tags['brand:en'] !== elements[0].tags['brand'] ? ` (${esc(elements[0].tags['brand:en'])})`: ''}` : '_not set_';
        const nameZh = brand ? esc(`${elements[0].tags.brand ?? brand}`) : 'unset';
        text += `## ${name} (${elements.length})\n`;
        textZh += `## ${nameZh} (${elements.length})\n`;
        if (brand) {
            text += `\`brand:wikidata\` = [${esc(brand)}](https://wikidata.org/wiki/${esc(brand)})\n`;
            textZh += `\`brand:wikidata\` = [${esc(brand)}](https://wikidata.org/wiki/${esc(brand)})\n`;
        }
        for (const element of elements.sort((a, b) => (a.tags['name:en'] ?? a.tags['name'] ?? '').localeCompare(b.tags['name:en'] ?? b.tags['name'] ?? ''))) {
            text += `* [${esc(element.tags['name:en'] ?? element.tags['name']) ?? 'no name'}](https://osm.org/${element.type}/${element.id})\n`;
            textZh += `* [${esc(element.tags['name']) ?? 'no name'}](https://osm.org/${element.type}/${element.id})\n`;
        }
    }

    return {
        text,
        textZh,
    }
}

function groupDuplicates(elements: OsmElement[]) {
    const arr: { distance: number, elements: OsmElement[] }[] = [];
    for (const element of elements) {
        if (element.tags.branch || element.tags.ref) {
            continue;
        }
        for (const elementB of elements) {
            if (element === elementB) {
                continue;
            }
            const distance = calcCrow(
                ...getSinglePositionFromTurboExpressElement(element),
                ...getSinglePositionFromTurboExpressElement(elementB)
            ) * 1000;
            if (distance < 40) {
                arr.push({
                    distance,
                    elements: [element, elementB],
                });
            }
        }
    }
    arr.sort((a, b) => a.distance - b.distance);
    
    // Deduplicate by creating a unique key for each pair
    const seen = new Set<string>();
    return arr.filter(pair => {
        const ids = pair.elements.map(e => e.id).sort().join('-');
        if (seen.has(ids)) {
            return false;
        }
        seen.add(ids);
        return true;
    });
}

export function duplicatesByPosition(elements: OsmElement[]) {
    const storesByBrand = {
        '7-Eleven': elements.filter(loose7ElevenFilter),
        'FamilyMart': elements.filter(looseFamilyMartFilter),
        'Hi-Life': elements.filter(looseHiLifeFilter),
        'OK Mart': elements.filter(looseOkMartFilter),
        'PxMart': elements.filter(loosePxmartFilter),
        'Simple Mart': elements.filter(looseSimpleMartFilter),
    };
    let dupsByBrand: Record<string, { distance: number, elements: OsmElement[] }[]> = {};
    for (const [key, stores] of Object.entries(storesByBrand)) {
        dupsByBrand[key] = groupDuplicates(stores);
    }

    const arr = [...Object.values(dupsByBrand)].flat();
    let text = '';
    if (arr.length === 0) {
        text += '✅ Yes, it does.';
    } else {
        text += `⚠️ No, there ${arr.length > 1 ? 'are' : 'is'} ${arr.length} store${arr.length > 1 ? 's' : ''} too close to another.`;
    }
    text += '\n\n<!--more-->\n\n';
    text += '## Downloads\n* [All duplicates as GPX](taiwan-grocery-stores-too-close.gpx)\n\n## Issues\n';
    //TODO Untranslated zh-tw string
    if (arr.length === 0) {
        text += 'Nice job! There are no apparent duplicates.';
    } else {
        for(const [brandName, dups] of Object.entries(dupsByBrand)) {
            text += `\n### ${brandName} (${dups.length})\n`;
            for (const pair of dups) {
                text += `#### ${pair.distance.toFixed(2)} meters apart\n`;
                for (const element of pair.elements) {
                    text += `* [${esc(element.tags['name:en'] ?? element.tags['name']) ?? 'no name'}](https://osm.org/${element.type}/${element.id})\n`;
                }
            }
        }
    }

    const gpx = osmElementToGpx(arr.flatMap(item => item.elements), e => e.tags['name:en'] ?? e.tags['name'] ?? 'no name', {
        name: 'Grocery stores in Taiwan which are too close to another',
        copyright: getOpenStreetMapCopyright(),
    });

    return {
        arr,
        text,
        files: {
            'taiwan-grocery-stores-too-close.gpx': gpx,
        },
    };
}

const loose7ElevenFilter = (e: OsmElement) => (
    e.tags['not:brand:wikidata'] !== 'Q259340' && (
        e.tags['name']?.toLocaleLowerCase().includes('7-eleven')
        || e.tags['name']?.toLocaleLowerCase().includes('7-11')
        || e.tags['name:en']?.toLocaleLowerCase().includes('7-eleven')
        || e.tags['name:en']?.toLocaleLowerCase().includes('7-11')
        || e.tags['brand']?.toLocaleLowerCase().includes('7-eleven')
        || e.tags['brand']?.toLocaleLowerCase().includes('7-11')
        || e.tags['brand:en']?.toLocaleLowerCase().includes('7-eleven')
        || e.tags['brand:en']?.toLocaleLowerCase().includes('7-11')
        || e.tags['brand:wikidata']?.toLocaleUpperCase() === 'Q259340'
    )
);

const looseFamilyMartFilter = (e: OsmElement) => (
    e.tags['not:brand:wikidata'] !== 'Q10891564' && (
        e.tags['name']?.toLocaleLowerCase().includes('familymart')
        || e.tags['name']?.toLocaleLowerCase().includes('family mart')
        || e.tags['name']?.includes('全家便利商店')
        || e.tags['name:en']?.toLocaleLowerCase().includes('familymart')
        || e.tags['name:en']?.toLocaleLowerCase().includes('family mart')
        || e.tags['name:zh']?.includes('全家便利商店')
        || e.tags['brand']?.toLocaleLowerCase().includes('familymart')
        || e.tags['brand']?.toLocaleLowerCase().includes('family mart')
        || e.tags['brand']?.includes('全家便利商店')
        || e.tags['brand:en']?.toLocaleLowerCase().includes('familymart')
        || e.tags['brand:en']?.toLocaleLowerCase().includes('family mart')
        || e.tags['brand:zh']?.includes('全家便利商店')
        || e.tags['brand:wikidata']?.toLocaleUpperCase() === 'Q10891564'
    )
);

const looseHiLifeFilter = (e: OsmElement) => (
    e.tags['not:brand:wikidata'] !== 'Q11326216' && (
        e.tags['name']?.toLocaleLowerCase().includes('hi-life')
        || e.tags['name']?.toLocaleLowerCase().includes('hilife')
        || e.tags['name:en']?.toLocaleLowerCase().includes('hi-life')
        || e.tags['name:en']?.toLocaleLowerCase().includes('hilife')
        || e.tags['brand']?.toLocaleLowerCase().includes('hi-life')
        || e.tags['brand']?.toLocaleLowerCase().includes('hilife')
        || e.tags['brand:en']?.toLocaleLowerCase().includes('hi-life')
        || e.tags['brand:en']?.toLocaleLowerCase().includes('hilife')
        || e.tags['brand:wikidata']?.toLocaleUpperCase() === 'Q11326216'
    )
);

const looseOkMartFilter = (e: OsmElement) => (
    e.tags['not:brand:wikidata'] !== 'Q10851968' && (
        e.tags['name']?.toLocaleLowerCase().startsWith('ok mart')
        || e.tags['name']?.toLocaleLowerCase().startsWith('okmart')
        || e.tags['name:en']?.toLocaleLowerCase().startsWith('ok mart')
        || e.tags['name:en']?.toLocaleLowerCase().startsWith('okmart')
        || e.tags['brand']?.toLocaleLowerCase().startsWith('ok mart')
        || e.tags['brand']?.toLocaleLowerCase().startsWith('okmart')
        || e.tags['brand:en']?.toLocaleLowerCase().startsWith('ok mart')
        || e.tags['brand:en']?.toLocaleLowerCase().startsWith('okmart')
        || e.tags['brand:wikidata']?.toLocaleUpperCase() === 'Q10851968'
    )
);

const loosePxmartFilter = (e: OsmElement) => (
    e.tags['not:brand:wikidata'] !== 'Q7262792' && (
        e.tags['name']?.toLocaleLowerCase().startsWith('pxmart')
        || e.tags['name']?.toLocaleLowerCase().startsWith('px mart')
        || e.tags['name:en']?.toLocaleLowerCase().startsWith('pxmart')
        || e.tags['name:en']?.toLocaleLowerCase().startsWith('px mart')
        || e.tags['brand']?.toLocaleLowerCase().startsWith('pxmart')
        || e.tags['brand']?.toLocaleLowerCase().startsWith('px mart')
        || e.tags['brand:en']?.toLocaleLowerCase().startsWith('pxmart')
        || e.tags['brand:en']?.toLocaleLowerCase().startsWith('px mart')
        || e.tags['brand:wikidata']?.toLocaleUpperCase() === 'Q7262792'
    )
);

const looseSimpleMartFilter = (e: OsmElement) => (
    e.tags['not:brand:wikidata'] !== 'Q15914017' && (
        e.tags['name']?.toLocaleLowerCase().includes('simple mart')
        || e.tags['name']?.toLocaleLowerCase().includes('simplemart')
        || e.tags['name:en']?.toLocaleLowerCase().includes('simple mart')
        || e.tags['name:en']?.toLocaleLowerCase().includes('simplemart')
        || e.tags['brand']?.toLocaleLowerCase().includes('simple mart')
        || e.tags['brand']?.toLocaleLowerCase().includes('simplemart')
        || e.tags['brand:en']?.toLocaleLowerCase().includes('simple mart')
        || e.tags['brand:en']?.toLocaleLowerCase().includes('simplemart')
        || e.tags['brand:wikidata']?.toLocaleUpperCase() === 'Q15914017'
    )
);

const looseWatsonsFilter = (e: OsmElement) => (
    e.tags['not:brand:wikidata'] !== 'Q7974785' && (
        e.tags['name']?.toLocaleLowerCase().includes('watson')
        || e.tags['name']?.includes('屈臣氏')
        || e.tags['name:en']?.toLocaleLowerCase().includes('watson')
        || e.tags['name:zh']?.includes('屈臣氏')
        || e.tags['brand']?.toLocaleLowerCase().includes('watson')
        || e.tags['brand']?.includes('屈臣氏')
        || e.tags['brand:en']?.toLocaleLowerCase().includes('watson')
        || e.tags['brand:zh']?.includes('屈臣氏')
        || e.tags['brand:wikidata']?.toLocaleUpperCase() === 'Q7974785'
    )
);

const looseFilters: Record<string, (e: OsmElement) => boolean | undefined> = {
    '7-eleven': loose7ElevenFilter,
    'family-mart': looseFamilyMartFilter,
    'hi-life': looseHiLifeFilter,
    'ok-mart': looseOkMartFilter,
    'pxmart': loosePxmartFilter,
    'simple-mart': looseSimpleMartFilter,
    'watsons': looseWatsonsFilter,
};

export function chainCompleteRefs(officialStores: NewOsmElement[], elements: OsmElement[], slug: string, name: string, nameZh: string, copyright: { author: string }) {
    const arr: {
        store: NewOsmElement,
        nearby: {
            distance: number,
            element: OsmElement | NewOsmElement,
        }[],
    }[] = [];

    const filter = looseFilters[slug];
    if (!filter) {
        throw Error(`No loose filter for ${slug}`);
    }
    const storeElements = elements.filter(filter);
    for (const store of officialStores) {
        if (store.type !== 'node') {
            console.dir(store);
            throw Error('Only nodes are expected');
        }
        if (elements.some(element => store.tags.ref === element.tags.ref)) {
            continue;
        }
        arr.push({
            store,
            nearby: findNearbyElements(store.lat, store.lon, storeElements),
        });
    }
    arr.sort((a, b) => Number.parseInt(a.store.tags.ref ?? '-1') - Number.parseInt(b.store.tags.ref ?? '-1'));

    let text = '';
    let textZh = '';
    if (arr.length === 0) {
        text += '✅ Yes, it can.';
        textZh += '✅ 是的，它可以。';
    } else {
        text += `⚠️ No, ${arr.length} ${name} store${arr.length > 1 ? 's are' : ' is'} missing.`;
        textZh += `⚠️ 不，有 ${arr.length} 個${nameZh}站點缺失。`;
    }
    text += '\n\n<!--more-->\n\n';
    textZh += '\n\n<!--more-->\n\n';
    text += `## Downloads\n* [All ${name} stores as GPX](taiwan-${slug}.gpx)\n* [Missing ${name} stores as GPX](taiwan-${slug}-not-on-osm-no-nearby.gpx)\n\n## Issues\n`;
    //TODO Untranslated zh-tw string
    textZh += `## 下載\n* [All ${nameZh} stores as GPX](taiwan-${slug}.gpx)\n* [Missing ${nameZh} stores as GPX](taiwan-${slug}-not-on-osm-no-nearby.gpx)\n\n## 數據\n`;
    if (arr.length === 0) {
        text += 'Nice job! There are no stores missing.';
        textZh += '不錯的工作！ 沒有遺失任何車站。';
    } else {
        for (const {store, nearby} of arr) {
            text += `* [${esc(store.tags.ref)}](https://osm.org/#map=19/${store.lat}/${store.lon})\n`;
            textZh += `* [${esc(store.tags.ref)}](https://osm.org/#map=19/${store.lat}/${store.lon})\n`;
            for (const {distance, element} of nearby) {
                let note = '';
                let noteZh = '';
                if (!element.tags.ref) {
                    note = ' (no ref)';
                    noteZh = ' (名稱匹配且沒有參考號)'; //TODO Update zh-tw translation
                }
                if (!!element.tags.ref && store.tags.ref !== element.tags.ref) {
                    note = ` (differing ref: ${esc(element.tags.ref)})`;
                    noteZh = ` (不同的參考: ${esc(element.tags.ref)})`;
                }
                if (!!element.tags.ref && store.tags.ref !== element.tags.ref) {
                    note = ` (differing branch name: ${element.tags.ref})`;
                    noteZh = ` (不同的參考: ${element.tags.ref})`;
                }
                text += `  * ${distance.toFixed(0)} m away: [${esc(element.tags['branch:en'] ?? element.tags.branch) ?? 'no name'}](https://osm.org/${element.type}/${element.id})${note}\n`;
                textZh += `  * ${distance.toFixed(0)}公尺外: [${esc(element.tags.branch ?? element.tags['branch:en']) ?? '無名'}](https://osm.org/${element.type}/${element.id})${noteZh}\n`;
            }
        }
    }

    const gpxAllStores = osmElementToGpx(officialStores, e => `${e.tags['branch']} (${e.tags['ref']})`, {
        name: `${name} stores`,
        copyright,
    });

    const noNearby = arr.filter(item => item.nearby.length === 0);
    const gpxNoNearby = osmElementToGpx(noNearby.map(item => item.store), e => `${e.tags['branch']} (${e.tags['ref']})`, {
        name: `${name} stores without a nearby store mapped on OSM`,
        copyright,
    });

    return {
        arr,
        files: {
            // TODO Untranslated zh-tw files
            [`taiwan-${slug}.gpx`]: gpxAllStores,
            [`taiwan-${slug}-not-on-osm-no-nearby.gpx`]: gpxNoNearby,
        },
        text,
        textZh,
    };
}

export function chainNoSurplus(officialStores: NewOsmElement[], elements: OsmElement[], slug: string, name: string) {
    const arr: {
        store: OsmElement,
        nearby: {
            distance: number,
            element: NewOsmElement,
        }[],
    }[] = [];

    const filter = looseFilters[slug];
    if (!filter) {
        throw Error(`No loose filter for ${slug}`);
    }
    const storeElements = elements.filter(filter);
    for (const store of storeElements) {
        if (!!store.tags.ref && officialStores.some(e => e.tags.ref === store.tags.ref)) {
            continue;
        }
        if (!!store.tags.branch && officialStores.some(e => (store.tags.branch !== e.tags.branch || store.tags.branch !== `${e.tags.branch}門市`))) {
            continue;
        }
        const [ lat, lon ] = getSinglePositionFromTurboExpressElement(store);
        arr.push({
            store,
            nearby: findNearbyElements(lat, lon, officialStores),
        });
    }
    arr.sort((a, b) => Number.parseInt(a.store.tags.ref ?? '-1') - Number.parseInt(b.store.tags.ref ?? '-1'));

    let text = '';
    if (arr.length === 0) {
        text += '✅ Yes, it can.';
    } else {
        text += `⚠️ No, ${arr.length} ${name} store${arr.length > 1 ? 's are' : ' is'} without an equivalent in the official dataset.`;
    }
    text += '\n\n<!--more-->\n\n';
    text += `## Downloads\n* [Surplus ${name} stores as GPX](taiwan-${slug}-only-on-osm.gpx)\n* [Surplus ${name} stores as GPX (no nearby stores)](taiwan-${slug}-only-on-osm-no-nearby.gpx)\n\n## Issues\n`;
    if (arr.length === 0) {
        text += 'Nice job! There are no surplus stores.';
    } else {
        for (const {store, nearby} of arr) {
            text += `* [${esc(store.tags.branch ?? store.tags.ref ?? store.tags['name:en'] ?? store.tags.name) ?? 'no branch name, ref, or name'}](https://osm.org/${store.type}/${store.id})\n`;
            for (const {distance, element} of nearby) {
                let note = '';
                if (!!store.tags.branch && (store.tags.branch !== element.tags.branch || store.tags.branch !== `${element.tags.branch}門市`)) {
                    note = ` (branch name differs)`;
                }
                if (!!store.tags.ref && store.tags.ref !== element.tags.ref) {
                    note = ` (ref differs)`;
                }
                if (element.type !== 'node') {
                    console.dir(store);
                    throw Error('Only nodes are expected');
                }
                text += `  * ${distance.toFixed(0)} m away: [${esc(element.tags.branch)} (${esc(element.tags.ref)})](https://osm.org/#map=19/${element.lat}/${element.lon})${note}\n`;
            }
        }
    }

    const gpxAllStores = osmElementToGpx(arr.map(o => o.store), e => e.tags['branch'] ?? e.tags['ref'] ?? e.tags['name'] ?? e.tags['name:en'] ?? e.id.toString(), {
        name: `${name} stores only on OSM`,
        copyright: getOpenStreetMapCopyright(),
    });

    const noNearby = arr.filter(item => item.nearby.length === 0);
    const gpxNoNearby = osmElementToGpx(noNearby.map(item => item.store), e => e.tags['branch'] ?? e.tags['ref'] ?? e.tags['name'] ?? e.tags['name:en'] ?? e.id.toString(), {
        name: `${name} stores without a nearby official store`,
    });

    return {
        arr,
        files: {
            [`taiwan-${slug}-only-on-osm.gpx`]: gpxAllStores,
            [`taiwan-${slug}-only-on-osm-no-nearby.gpx`]: gpxNoNearby,
        },
        text,
    };
}
