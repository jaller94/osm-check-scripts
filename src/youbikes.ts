import { calcCrow } from './helper';
import { findNearbyElements, getSinglePositionFromTurboExpressElement } from './helper-geo';
import { OsmElement } from './types';

// export function findByRef(elements: Record<string, any>[], ref: string) {
//     return elements.;
// }

type YouBikeStation = {
    station_no: string,
    lat: string,
    lng: string,
    name_tw: string,
} & Record<string, unknown>;

export function completeRefs(stations: YouBikeStation[], elements: OsmElement[]) {
    const arr: {
        station: YouBikeStation
        nearby: {
            distance: number,
            element: OsmElement,
        }[],
    }[] = [];

    for (const station of stations) {
        const exists = elements.findIndex(element => station.station_no === element.tags.ref) !== -1;
        if (exists) {
            continue;
        }
        arr.push({
            station,
            nearby: findNearbyElements(Number.parseFloat(station.lat), Number.parseFloat(station.lng), elements),
        });
    }
    arr.sort((a, b) => Number.parseInt(a.station.station_no) - Number.parseInt(b.station.station_no));

    let text = '';
    let textZh = '';
    if (arr.length === 0) {
        text += '✅ Yes, it can.';
        textZh += '✅ 是的，它可以。';
    } else {
        text += `⚠️ No, there ${arr.length > 1 ? 'are' : 'is'} ${arr.length} YouBike station${arr.length > 1 ? 's' : ''} missing.`;
        textZh += `⚠️ 不，有 ${arr.length} 個 YouBike 站點缺失。`;
    }
    text += '\n\n<!--more-->\n\n';
    textZh += '\n\n<!--more-->\n\n';
    if (arr.length === 0) {
        text += 'Nice job! There are no stations missing.';
        textZh += '不錯的工作！ 沒有遺失任何車站。';
    } else {
        for (const {station, nearby} of arr) {
            text += `* [${station.station_no}: ${station.name_en ?? 'no name'}](https://osm.org/#map=19/${station.lat.trim()}/${station.lng.trim()})\n`;
            textZh += `* [${station.station_no}: ${station.name_tw ?? '無名'}](https://osm.org/#map=19/${station.lat.trim()}/${station.lng.trim()})\n`;
            for (const {distance, element} of nearby) {
                let note = '';
                let noteZh = '';
                if (station.name_tw === element.tags.name) {
                    note = ' (the name matches and it has no ref)';
                    noteZh = ' (名稱匹配且沒有參考號)';
                }
                if (!!element.tags.ref && station.station_no !== element.tags.ref) {
                    note = ` (differing ref: ${element.tags.ref})`;
                    noteZh = ` (不同的參考: ${element.tags.ref})`;
                    if (station.name_tw === element.tags.name) {
                        note = ` (the name matches, differing ref: ${element.tags.ref})`;
                        noteZh = ` (名稱匹配，但參考不同： ${element.tags.ref})`;
                    }
                }
                if (station.station_no.length === 4 && /^\d{9}$/.test(element.tags.ref)) {
                    note = ' (but this looks like a YouBike 2.0 station)';
                    noteZh = ' (但這看起來像是 YouBike 2.0 站)';
                }
                if (station.station_no.length === 9 && /^\d{4}$/.test(element.tags.ref)) {
                    note = ' (but this looks like a YouBike 1.0 station)';
                    noteZh = ' (但這看起來像是 YouBike 1.0 站)';
                }
                text += `  * ${distance.toFixed(0)} m away: [${element.tags['name:en'] ?? element.tags.name ?? 'no name'}](https://osm.org/${element.type}/${element.id})${note}\n`;
                textZh += `  * ${distance.toFixed(0)}公尺外: [${element.tags.name ?? '無名'}](https://osm.org/${element.type}/${element.id})${noteZh}\n`;
            }
        }
    }
    return {
        arr,
        files: {
            'youbike-all-official-stations.gpx': stationsToGpx(stations),
        },
        text,
        textZh,
    };
}

export function noSurplus(stations: YouBikeStation[], elements: OsmElement[]) {
    const arr: OsmElement[] = [];
    for (const element of elements) {
        if (!element.tags.ref) {
            continue;
        }
        const exists = stations.some(station => station.station_no === element.tags.ref);
        if (exists) {
            continue;
        }
        arr.push(element);
    }
    arr.sort((a, b) => a.tags.ref.localeCompare(b.tags.ref));

    let text = '';
    if (arr.length === 0) {
        text += '✅ Yes, it can.';
    } else {
        text += `⚠️ No, there ${arr.length > 1 ? 'are' : 'is'} ${arr.length} YouBike station${arr.length > 1 ? 's' : ''} without an equivalent in the official dataset.`;
    }
    text += '\n\n<!--more-->\n\n';
    if (arr.length === 0) {
        text += 'Nice job! There are no additional stations in OSM.';
    } else {
        for (const element of arr) {
            text += `* [${element.tags.ref}: ${element.tags.name ?? 'no name'}](https://osm.org/${element.type}/${element.id})\n`;
        }
    }
    return {
        arr,
        text,
    };
}

export function completeTags(elements: OsmElement[]) {
    const requiredTags = 'amenity,authentication:contactless,authentication:membership_card,bicycle_rental,capacity,fee,name,name:en,name:zh,name:zh-Hant,network:wikidata,opening_hours,payment:credit_cards,payment:ep_easycard,payment:ep_ipass,payment:jcb,payment:mastercard,payment:visa,ref,rental'.split(',');
    const arr: {
        element: OsmElement,
        missingTags: string[],
    }[] = [];
    for (const element of elements) {
        if (
            element.tags['amenity'] !== 'bicycle_rental'
            && element.tags['bicycle_rental'] !== 'docking_station'
        ) {
            continue;
        }
        if (
            element.tags.brand?.toLocaleLowerCase().startsWith('youbike')
            && element.tags['network:wikidata'] !== 'Q20687952'
            && element.tags['network'] !== 'YouBike 微笑單車'
            && element.tags['network'] !== 'YouBike'
        ) {
            continue;
        }
        const missingTags: string[] = [];
        for (const tag of requiredTags) {
            if (typeof element.tags[tag] === 'string') {
                continue;
            }
            missingTags.push(tag);
        }
        if (missingTags.length > 0) {
            arr.push({
                element,
                missingTags,
            });
        }
    }
    arr.sort((a, b) => a.element.id - b.element.id);

    let text = '';
    if (arr.length === 0) {
        text += '✅ Yes, it does.';
    } else {
        text += `⚠️ No, there ${arr.length > 1 ? 'are' : 'is'} ${arr.length} YouBike station${arr.length > 1 ? 's' : ''} with incomplete tags.`;
    }
    text += '\n\n<!--more-->\n\n';
    if (arr.length === 0) {
        text += 'Most impressive! There are no missing tags on stations in OSM.';
    } else {
        for (const {element, missingTags} of arr) {
            text += `* [${element.tags['name:en'] ?? element.tags.name ?? 'no name'}](https://osm.org/${element.type}/${element.id})\n`;
            for (const tag of missingTags) {
                text += `  * ${tag}\n`;
            }
        }
    }
    return {
        arr,
        text,
    };
}

export function correctTags(elements: OsmElement[]) {
    const arr: {
        element: OsmElement,
        issues: string[],
    }[] = [];
    for (const element of elements) {
        if (
            element.tags['amenity'] !== 'bicycle_rental'
            && element.tags['bicycle_rental'] !== 'docking_station'
        ) {
            continue;
        }
        if (
            element.tags.brand?.toLocaleLowerCase().startsWith('youbike')
            && element.tags['network:wikidata'] !== 'Q20687952'
            && element.tags['network'] !== 'YouBike 微笑單車'
            && element.tags['network'] !== 'YouBike'
        ) {
            continue;
        }
        const issues: string[] = [];
        
        if (element.tags['capacity'] && !/;/.test(element.tags['capacity']) && !/^1?\d{1,2}$/.test(element.tags['capacity'])) {
            issues.push(`\`capacity\` isn't a number between 0 and 199 (currently: ${element.tags['capacity']})`);
        }
        if (element.tags['fee'] && !/^yes$/.test(element.tags['fee'])) {
            issues.push(`\`fee\` isn't "yes" (currently: ${element.tags['fee']})`);
        }
        if (element.tags['name'] && /^[a-zA-Z0-9 \(\)\.,&\/]+$/.test(element.tags['name'])) {
            issues.push(`\`name\` isn't in Chinese (currently: ${element.tags['name']})`);
        }
        if (element.tags['network:wikidata'] && !/^Q\d+$/.test(element.tags['network:wikidata'])) {
            issues.push(`\`network:wikidata\` isn't in the format Q123 (currently: ${element.tags['capacity']})`);
        }

        if (issues.length > 0) {
            arr.push({
                element,
                issues,
            });
        }
    }
    arr.sort((a, b) => a.element.id - b.element.id);

    let text = '';
    if (arr.length === 0) {
        text += '✅ Yes, it does.';
    } else {
        text += `⚠️ No, there ${arr.length > 1 ? 'are' : 'is'} ${arr.length} YouBike station${arr.length > 1 ? 's' : ''} with invalid values.`;
    }
    text += '\n\n<!--more-->\n\n';
    if (arr.length === 0) {
        text += 'Nice work! No invalid values on stations in OSM have been found.';
    } else {
        for (const {element, issues} of arr) {
            text += `* [${element.tags['name:en'] ?? element.tags.name ?? 'no name'}](https://osm.org/${element.type}/${element.id})\n`;
            for (const issue of issues) {
                text += `  * ${issue}\n`;
            }
        }
    }
    return {
        arr,
        text,
    };
}

export function duplicateRefs(elements: OsmElement[]): { arr: OsmElement[], text: string } {
    const map: Map<string, OsmElement[]> = new Map();
    for (const element of elements) {
        if (!element.tags.ref) {
            continue;
        }
        map.set(element.tags.ref, [
            ...(map.get(element.tags.ref) ?? []),
            element,
        ]);
    }

    const arr: OsmElement[] = [];
    for (const [ref, elements] of map.entries()) {
        if (elements.length > 1) {
            arr.push(...elements);
        } else {
            map.delete(ref);
        }
    }
    
    let text = '';
    if (map.size === 0) {
        text += '✅ Yes, it does.';
    } else {
        text += `⚠️ No, there ${map.size > 1 ? 'are' : 'is'} ${map.size} value${map.size > 1 ? 's' : ''} being reused.`;
    }
    text += '\n\n<!--more-->\n\n';
    if (map.size === 0) {
        text += 'Nice job! There are no apparent duplicates.';
    } else {
        const sorted = [...map.entries()];
        sorted.sort((a, b) => a[0].localeCompare(b[0]));
        for (const [ref, elements] of sorted) {
            if (elements.length === 1) {
                continue;
            }
            text += `## ${ref}\n`;
            for (const element of elements) {
                text += `* [${element.tags['name:en'] ?? element.tags.name ?? 'no name'}](https://osm.org/${element.type}/${element.id})\n`;
            }
        }
    }
    return {
        arr,
        text,
    };
}

export function missingRef(elements: OsmElement[]): { arr: OsmElement[], text: string } {
    const arr: OsmElement[] = [];
    for (const element of elements) {
        if (element.tags.ref) {
            continue;
        }
        arr.push(element);
    }
    
    let text = '';
    if (arr.length === 0) {
        text += '✅ Yes, it does.';
    } else {
        text += `⚠️ No, ${arr.length} station${arr.length > 1 ? 's' : ''} ${arr.length > 1 ? 'have' : 'has'} no ref=* tag.`;
    }
    text += '\n\n<!--more-->\n\n';
    if (arr.length === 0) {
        text += 'Most impressive! There are no ref=* tags missing.';
    } else {
        for (const element of arr) {
            text += `* [${element.tags['name:en'] ?? element.tags.name ?? 'no name'}](https://osm.org/${element.type}/${element.id})\n`;
        }
    }
    return {
        arr,
        text,
    };
}

export function duplicatePosition(elements: OsmElement[]): { arr: { distance: number, elements: OsmElement[] }[], text: string } {
    const arr: { distance: number, elements: OsmElement[] }[] = [];
    for (const element of elements) {
        if (element.tags.ref) {
            continue;
        }
        for (const elementB of elements) {
            if (element === elementB) {
                continue;
            }
            const distance = calcCrow(
                ...getSinglePositionFromTurboExpressElement(element),
                ...getSinglePositionFromTurboExpressElement(elementB)
            ) * 1000;
            if (distance < 40) {
                arr.push({
                    distance,
                    elements: [element, elementB],
                });
            }
        }
    }

    arr.sort((a, b) => a.distance - b.distance);

    let text = '';
    if (arr.length === 0) {
        text += '✅ Yes, it does.';
    } else {
        text += `⚠️ No, there ${arr.length > 1 ? 'are' : 'is'} ${arr.length} station${arr.length > 1 ? 's' : ''} too close to another.`;
    }
    text += '\n\n<!--more-->\n\n';
    if (arr.length === 0) {
        text += 'Nice job! There are no apparent duplicates.';
    } else {
        for (const pair of arr) {
            text += `## ${pair.distance.toFixed(2)} meters apart\n`;
            for (const element of pair.elements) {
                text += `* [${element.tags['name:en'] ?? element.tags['name'] ?? 'no name'}](https://osm.org/${element.type}/${element.id})\n`;
            }
        }
    }
    return {
        arr,
        text,
    };
}

export function differentPosition(stations: YouBikeStation[], elements: OsmElement[]) {
    const arr: {
        distance: number,
        element: OsmElement,
        station: YouBikeStation,
    }[] = [];
    for (const element of elements) {
        if (!element.tags.ref) {
            continue;
        }
        const station = stations.find(station => station.station_no === element.tags.ref);
        if (!station) {
            continue;
        }
        const distance = calcCrow(
            ...getSinglePositionFromTurboExpressElement(element),
            Number.parseFloat(station.lat), Number.parseFloat(station.lng),
        ) * 1000;
        if (distance > 80) {
            arr.push({
                distance,
                element,
                station,
            });
        }
    }

    arr.sort((a, b) => a.distance - b.distance);

    let text = '';
    if (arr.length === 0) {
        text += '✅ Yes, it is.';
    } else {
        text += `⚠️ No, there ${arr.length > 1 ? 'are' : 'is'} ${arr.length} station${arr.length > 1 ? 's' : ''} not in the reported position.`;
    }
    text += '\n\n<!--more-->\n\n';
    if (arr.length === 0) {
        text += 'Nice job! All identifiable stations are at the correct location.';
    } else {
        for (const {distance, element, station} of arr) {
            text += `## ${distance.toFixed(2)} meters away: ${station.station_no} ${station.name_en}\n`;
            text += `* [${element.tags['name:en'] ?? element.tags['name'] ?? 'no name'} on OSM](https://osm.org/${element.type}/${element.id})\n`;
            text += `* [${station.name_en} on the API](https://osm.org/#map=19/${station.lat.trim()}/${station.lng.trim()})\n`;
        }
    }
    return {
        arr,
        text,
    };
}

const stationsToGpx = (stations: YouBikeStation[]): string => {
    let text = `<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n`;
    text += `<gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1"\n`
    text += `xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n`;
    text += `xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">\n`;
    for (const station of stations) {
        text += `<wpt lat="${station.lat}" lon="${station.lng}"><name>${station.name_tw} (${station.station_no})</name></wpt>\n`;
    }
    text += '</gpx>\n';
    return text;
};
