import z from 'zod';
import { cleanNode, convertToNode, stolpersteinType } from './downloaders/stolpersteine-berlin';
import { calcCrow, esc, groupBy } from './helper';
import { findNearbyElements, osmElementToGpx } from './helper-geo';
import { NewOsmElement, OsmElement } from './types';

export function stolpersteineBerlinSummary(elements: OsmElement[]) {
    const noName = elements.filter(e => e.tags.name === undefined);
    const noInscription = elements.filter(e => e.tags.inscription === undefined);
    const noWebsite = elements.filter(e => e.tags.website === undefined);
    let text = 'An analysis of stolpersteine on OpenStreetMap in Berlin.\n';
    let textDe = 'Eine Analyse von Stolpersteinen auf OpenStreetMap in Berlin.\n';
    text += `<!--more-->\n`;
    textDe += `<!--more-->\n`;
    text += `* ${elements.length} stolpersteine\n`;
    textDe += `* ${elements.length} Stolpersteine\n`;

    text += `## Missing tags\n`;
    textDe += `## Fehlende Tags\n`;
    text += `* ${noName.length} without a name\n`;
    textDe += `* ${noName.length} ohne Namen\n`;
    text += `* ${noInscription.length} without an inscription\n`;
    textDe += `* ${noInscription.length} ohne Inschrift (\`inscription\`)\n`;
    text += `* ${noWebsite.length} without a website\n`;
    textDe += `* ${noWebsite.length} ohne Webseite\n`;

    text += `## High-quality tags\n`;
    textDe += `## Großartige Tags\n`;
    const hasWikipedia = elements.filter(e => e.tags.wikipedia !== undefined);
    const hasWikidata = elements.filter(e => e.tags.wikidata !== undefined);
    const hasSubjectWikidata = elements.filter(e => e.tags['subject:wikidata'] !== undefined);
    text += `* ${hasWikipedia.length} link to a Wikipedia article\n`;
    textDe += `* ${hasWikipedia.length} verlinken auf einen Wikipedia-Artikel\n`;
    text += `* ${hasWikidata.length} link to a Wikidata entry of the stolperstein\n`;
    textDe += `* ${hasWikidata.length} verlinken einen Wikidata-Eintrag zum Stolperstein\n`;
    text += `* ${hasSubjectWikidata.length} link to a Wikidata entry of the person\n`;
    textDe += `* ${hasSubjectWikidata.length} verlinken einen Wikidata-Eintrag zur Person\n`;
    
    const hasUrl = elements.filter(e => e.tags.url !== undefined);
    const hasAddr = elements.filter(e => Object.keys(e.tags).findIndex(tag => tag.startsWith('addr:')) !== -1);
    text += `## Tags to avoid\n`;
    textDe += `## Unerwünschte Tags\n`;
    text += `* ${hasUrl.length} have an URL (please use \`website\` or \`image\`)\n`;
    textDe += `* ${hasUrl.length} haben eine URL (bitte \`website\` oder \`image\` benutzen)\n`;
    text += `* ${hasAddr.length} have an \`addr:*\` (please use \`memorial:addr:*\`)\n`;
    textDe += `* ${hasAddr.length} hat einen \`addr:*\`-Wert (bitte \`memorial:addr:*\` verwenden)\n`;

    text += `## No name\n`;
    textDe += `## Kein Name\n`;
    for (const element of noName) {
        text += `* [${element.id}](https://osm.org/${element.type}/${element.id})\n`;
        textDe += `* [${element.id}](https://osm.org/${element.type}/${element.id})\n`;
    }
    text += `## Grouped by website\n`;
    textDe += `## Nach Webseite gruppiert\n`;
    const byWebsite = groupBy(elements, e => e.tags.website);
    for (const [url, duplicates] of byWebsite.entries()) {
        if (!url || duplicates.length < 2) {
            continue;
        }
        text += `* ${esc(url)} (${duplicates.length} times)\n`;
        textDe += `* ${esc(url)} (${duplicates.length} mal)\n`;
        for (const element of duplicates) {
            text += `  * [${esc(element.tags.name) ?? element.id}](https://osm.org/${element.type}/${element.id})\n`;
            textDe += `  * [${esc(element.tags.name) ?? element.id}](https://osm.org/${element.type}/${element.id})\n`;
        }
    }
    text += `## Grouped by URL\n`;
    textDe += `## Nach URL gruppiert\n`;
    const byUrl = groupBy(elements, e => e.tags.url);
    for (const [url, duplicates] of byUrl.entries()) {
        if (!url || duplicates.length < 2) {
            continue;
        }
        text += `* ${esc(url)} (${duplicates.length} times)\n`;
        textDe += `* ${esc(url)} (${duplicates.length} mal)\n`;
        for (const element of duplicates) {
            text += `  * [${esc(element.tags.name) ?? element.id}](https://osm.org/${element.type}/${element.id})\n`;
            textDe += `  * [${esc(element.tags.name) ?? element.id}](https://osm.org/${element.type}/${element.id})\n`;
        }
    }
    return {
        text,
        textDe,
    };
}

const generateDescription = (e: OsmElement | NewOsmElement) => {
    let description = `${e.tags.name}, ${e.tags['memorial:addr:street']}`
    if (e.tags['memorial:addr:housenumber']) {
        description += ` ${e.tags['memorial:addr:housenumber']}`;
    }
    description += `, ${e.tags.website}`;
    return description;
};

const hardcodedMatches = new Map<string, number>([
    ['571', 1769772596], // Leo Rosshändler / Leopold Rosshändler
    ['743', 12036393932], // Maria / Marja
    ['1367', 1329982253],
    ['2023', 789349587], // Else / Elsa
    ['2039', 12358160591], // Hanne / Channe
    ['2079', 12062153372], // Jaekel / Jaeckel
    ['2417', 1117306896], // Joachim Max Aronade / Joachim M. Aronade
    ['3386', 12473728378], // Hermann Wechsler (Vecsler)
    ['3387', 12473728379], // Johanna Wechsler (Vecsler)
    ['695', 12068850529], // Miriam / Mirjam
    ['3488', 12062160116], // Debora / Deborah
    ['3581', 7289365685], // Salomon Fritz Herz / Fritz Herz
    ['3756', 1852943263], // Dorris / Doris
    ['4707', 6262678724], // Vera Ernestine Birkenfeld / Vera E. Birkenfeld
    ['5314', 11432307543], // Freibusch / Feibusch
    ['5680', 12138227146], // Klara / Clara
    ['5771', 12135654695], // Schoenbeck / Schönbeck
    ['7285', 12062160122], // Leib Leo Eimer
    ['7487', 12062153373], // Jacob / Jakob
    ['9279', 12391630124], // Alfred Abraham Endsil Kahan
    ['9422', 12351407114], // Bertha / Berta
    ['9423', 12351407116], // Lieselotte / Lilo
    ['9492', 12351407115], // Peter Hirschweh / Peter Edel-Hirschweh
    ['9713', 10313113527], // Alice Essinger
    ['9714', 10313113526], // Selma Rosengarten, verw. Hecht
    ['9743', 9558386667], // Clara “Cläre” Semmel
    ['9923', 8541512434], // Friedrich Ludwig Epstein / Friedrich L. Epstein
    ['10418', 12351382483], // Luise Cäcilia Smedresman 
    ['10422', 12351382479], // Ruth Smedresman
    ['10545', 12596170801], // Karolin / Karoline
    ['10582', 12068944082], // Naftali Stern / Naftali Natan Stern
    ['11011', 12068929637], // Lola Lea Lotte Kempler / Lola Lea Kempler
]);

const regExp = /^https:\/\/www\.stolpersteine-berlin\.de\/([a-z]{2}\/)?biografie\/(\d+)$/;
function parseBiografieURL(url?: string) {
    const match = url?.match(regExp);
    return {
        biografieId: match?.[2],
    };
}

export function stolpersteineBerlinComplete(officialStolpersteine: NewOsmElement[], elements: OsmElement[]) {
    const arr: {
        stolperstein: NewOsmElement,
        nearby: {
            distance: number,
            element: OsmElement,
        }[],
    }[] = [];

    
    const processedOfficialStolpersteine = officialStolpersteine.map(stolperstein => {
        const biographyId = parseBiografieURL(stolperstein.tags.website)?.biografieId;
        return {
            biographyId,
            name: stolperstein.tags.name?.toLocaleLowerCase().replace(/^dr\. /, '').replace(/,? (geb\.?|gen\.|genannt|verw\.) .+$/, '').replace(' (künstlername)', '').replace('é', 'e').replace('owsky','owski').replace(/'"„“/g, ''),
            value: stolperstein,
            matched: biographyId ? hardcodedMatches.has(biographyId) : false,
        }
    });
    const processedElements = elements.map(element => ({
        biographyId: parseBiografieURL(element.tags.website)?.biografieId,
        name: element.tags.name?.toLocaleLowerCase().replace(/^dr\. /, '').replace(/,? (geb\.?|gen\.|genannt|verw\.) .+$/, '').replace(' (künstlername)', '').replace('é', 'e').replace('owsky','owski').replace(/'"„“/g, ''),
        altName: element.tags.alt_name?.toLocaleLowerCase().replace(/^dr\. /, '').replace(/,? (geb\.?|gen\.|genannt|verw\.) .+$/, '').replace(' (künstlername)', '').replace('é', 'e').replace('owsky','owski').replace(/'"„“/g, ''),
        value: element,
        matched: [...hardcodedMatches.values()].includes(element.id),
    }));

    const osmStolpersteineWithoutAMatch: OsmElement[] = [];
    for (const element of processedElements) {
        if (element.matched) {
            continue;
        }
        if (element.biographyId && processedOfficialStolpersteine.some(stolperstein => stolperstein.biographyId === element.biographyId)) {
            continue;
        }
        if (!element.biographyId && element.name && processedOfficialStolpersteine.some(stolperstein => stolperstein.name === element.name || stolperstein.name === element.altName)) {
            continue;
        }
        osmStolpersteineWithoutAMatch.push(element.value);
    }

    for (const stolperstein of processedOfficialStolpersteine) {
        if (stolperstein.value.type !== 'node') {
            throw Error('Only nodes are expected');
        }
        if (stolperstein.matched || processedElements.some(element => {
            if (element.matched) {
                return false;
            }
            if (element.biographyId && element.biographyId === stolperstein.biographyId) {
                return true;
            }
            // Match nearby names
            if (element.value.type === 'node' && stolperstein.value.type === 'node' && calcCrow(element.value.lat, element.value.lon, stolperstein.value.lat, stolperstein.value.lon) < 60) {
                return stolperstein.name === element.name || stolperstein.name === element.altName || stolperstein.name?.replace(/ .+ /, ' ') === element.name;
            }
            return false;
        })) {
            continue;
        }
        arr.push({
            stolperstein: stolperstein.value,
            nearby: findNearbyElements(stolperstein.value.lat, stolperstein.value.lon, osmStolpersteineWithoutAMatch),
        });
    }
    // arr.sort((a, b) => Number.parseInt(a.stolperstein.tags.ref) - Number.parseInt(b.stolperstein.tags.ref));

    const noNearby = arr.filter(item => item.nearby.length === 0).map(item => item.stolperstein);

    let text = '';
    let textDe = '';
    if (arr.length === 0) {
        text += '✅ Yes, it can.';
        textDe += '✅ Ja, alle sind eingetragen.';
    } else {
        text += `⚠️ No, at least ${arr.length} stolperstein${arr.length > 1 ? 'e are' : ' is'} missing on OSM.`;
        textDe += `⚠️ Nein, noch mindestens ${arr.length} Stolperstein${arr.length > 1 ? 'e fehlen' : ' fehlt'} auf OSM.`;
    }
    text += '\n\n<!--more-->\n\n';
    textDe += '\n\n<!--more-->\n\n';
    text += `## Downloads\n* [All ${officialStolpersteine.length} stolpersteine as GPX](stolpersteine-berlin.gpx)\n* [${noNearby.length} stolpersteine without nearby candidates on OSM as GPX](stolpersteine-berlin-not-on-osm-no-nearby.gpx)\n\n`;
    textDe += `## Downloads\n* [Alle ${officialStolpersteine.length} Stolpersteine als GPX](stolpersteine-berlin.gpx)\n* [${noNearby.length} Stolpersteine ohne nahe Kandidaten in OSM als GPX](stolpersteine-berlin-not-on-osm-no-nearby.gpx)\n\n`;
    text += `## Sources\nThis report compares data from OpenStreetMap with a dataset from [stolpersteine-berlin.de](https://www.stolpersteine-berlin.de).\n\nThe data gets compared to OSM using [TypeScript code](https://gitlab.com/jaller94/osm-check-scripts/-/blob/main/src/stolpersteine.ts).\n\n## Issues\n`;
    textDe += `## Datenquellen\nDieser Report vergleicht die Daten auf OpenStreetMap mit einem Datensatz von [stolpersteine-berlin.de](https://www.stolpersteine-berlin.de).\n\nDie Daten werden mit diesem [TypeScript-Quelltext](https://gitlab.com/jaller94/osm-check-scripts/-/blob/main/src/stolpersteine.ts) mit OSM verglichen.\n\n## Probleme\n`;
    if (arr.length === 0) {
        text += 'Nice job! There are no stolpersteine missing on OSM.';
        textDe += 'Gut gemacht! Es fehlen keine Stolpersteine auf OSM.';
    } else {
        for (const {stolperstein, nearby} of arr) {
            text += `* ${esc(stolperstein.tags.name)} | [Map](https://osm.org/#map=19/${stolperstein.lat}/${stolperstein.lon}) | [Biography](${stolperstein.tags.website})\n`;
            textDe += `* ${esc(stolperstein.tags.name)} | [Karte](https://osm.org/#map=19/${stolperstein.lat}/${stolperstein.lon}) | [Biografie](${stolperstein.tags.website})\n`;
            for (const {distance, element} of nearby) {
                let note = '';
                let noteDe = '';
                if (!element.tags.name) {
                    note = ' (no name)';
                    noteDe = ' (kein Name)';
                }
                if (stolperstein.tags.name !== element.tags.name && stolperstein.tags.name !== element.tags.alt_name) {
                    note = ` (differing name)`;
                    noteDe = ` (abweichender Name)`;
                } else if (!!element.tags.website && stolperstein.tags.website !== element.tags.website) {
                    note = ` (differing website: ${esc(element.tags.website)})`;
                    noteDe = ` (abweichende Webseite: ${esc(element.tags.website)})`;
                }
                text += `  * ${distance.toFixed(0)} m away: [${esc(element.tags.name ?? element.tags.website ?? 'no name')}](https://osm.org/${element.type}/${element.id})${note}\n`;
                textDe += `  * ${distance.toFixed(0)} m entfernt: [${esc(element.tags.name ?? element.tags.website ?? 'kein Name')}](https://osm.org/${element.type}/${element.id})${noteDe}\n`;
            }
        }
    }

    const copyright = {
        author: 'Koordinierungsstelle Stolpersteine Berlin',
    };

    const gpxAllStolperstein = osmElementToGpx(officialStolpersteine, generateDescription, {
        name: `Stolpersteine Berlin`,
        copyright,
    });

    const gpxNoNearby = osmElementToGpx(noNearby, generateDescription, {
        name: `Stolpersteine without a nearby stolperstein mapped on OSM`,
        copyright,
    });

    return {
        arr,
        files: {
            [`stolpersteine-berlin.gpx`]: gpxAllStolperstein,
            [`stolpersteine-berlin-not-on-osm-no-nearby.gpx`]: gpxNoNearby,
        },
        text,
        textDe,
    };
}

export function stolpersteineBerlinCompleteByLocalDistrict(officialStolpersteine: z.infer<typeof stolpersteinType>[], osmStolpersteineBerlin: OsmElement[]) {
    let byLocalDistrict: Map<string, {
        missing: z.infer<typeof stolpersteinType>[],
        all: z.infer<typeof stolpersteinType>[],
    }> = new Map();

    const processedOfficialStolpersteine = officialStolpersteine.map(cleanNode);

    for (const stolperstein of processedOfficialStolpersteine) {
        let localDistrict = byLocalDistrict.get(stolperstein.localdistrict);
        if (!localDistrict) {
            localDistrict = {
                missing: [],
                all: [],
            };
            byLocalDistrict.set(stolperstein.localdistrict, localDistrict);
        }
        const biografieId = parseBiografieURL(stolperstein.url)?.biografieId;
        if (biografieId && hardcodedMatches.has(stolperstein.url)) {
            localDistrict.all.push(stolperstein);
            return;
        }
        const within20m = findNearbyElements(stolperstein.lat, stolperstein.lon, osmStolpersteineBerlin, 20);
        const within100m = findNearbyElements(stolperstein.lat, stolperstein.lon, osmStolpersteineBerlin, 100);
        const existsWithin100m = stolperstein.name && within100m.some(osmStolperstein => {
            const stolpersteinCleanName = stolperstein.name.toLocaleLowerCase().replace(/^dr\. /, '').replace(/,? (geb\.?|gen\.|genannt|verw\.) .+$/, '').replace(' (künstlername)', '').replace('é', 'e').replace('owsky','owski').replace(/'"„“/g, '');
            const osmStolpersteinCleanName = osmStolperstein.element.tags.name?.toLocaleLowerCase().replace(/^dr\. /, '').replace(/,? (geb\.?|gen\.|genannt|verw\.) .+$/, '').replace(' (künstlername)', '').replace('é', 'e').replace('owsky','owski').replace(/'"„“/g, '');
            const osmStolpersteinCleanAltName = osmStolperstein.element.tags.alt_name?.toLocaleLowerCase().replace(/^dr\. /, '').replace(/,? (geb\.?|gen\.|genannt|verw\.) .+$/, '').replace(' (künstlername)', '').replace('é', 'e').replace('owsky','owski').replace(/'"„“/g, '');
            return stolpersteinCleanName === osmStolpersteinCleanName
                || stolpersteinCleanName === osmStolpersteinCleanAltName
                || stolperstein.url === osmStolperstein.element.tags.website
                || stolperstein.url === osmStolperstein.element.tags.url;
        });
        if (within20m.length === 0 && !existsWithin100m) {
            localDistrict.missing.push(stolperstein);
        }
        localDistrict.all.push(stolperstein);
    }

    // Sort alphabetically
    byLocalDistrict = new Map(
        [...byLocalDistrict.entries()].sort((a, b) => a[0].localeCompare(b[0]))
    );
    
    let text = '';
    let textDe = '';

    for (const [localDistrictName, localDistrict] of byLocalDistrict.entries()) {
        text += `* <progress value="${localDistrict.all.length - localDistrict.missing.length}" max="${localDistrict.all.length}"></progress> ${localDistrictName}`;
        textDe += `* <progress value="${localDistrict.all.length - localDistrict.missing.length}" max="${localDistrict.all.length}"></progress> ${localDistrictName}`;
        text += ` (missing ${localDistrict.missing.length} of ${localDistrict.all.length})`;
        textDe += ` (es ${localDistrict.missing.length === 1 ? 'fehlt' : 'fehlen'} ${localDistrict.missing.length} von ${localDistrict.all.length})`;
        if (localDistrict.missing.length > 0) {
            text += ` [GPX](stolpersteine-berlin-not-on-osm-no-nearby-${localDistrictName.replace(/ /g,'-').toLocaleLowerCase()}.gpx)`;
            textDe += ` [GPX](stolpersteine-berlin-not-on-osm-no-nearby-${localDistrictName.replace(/ /g,'-').toLocaleLowerCase()}.gpx)`;
        }
        text += '\n';
        textDe += '\n';
    }

    const files: Record<string, string> = {};
    const copyright = {
        author: 'Koordinierungsstelle Stolpersteine Berlin',
    };
    const date = new Date();
    const dateString = `${date.getUTCFullYear()}-${(date.getUTCMonth() + 1).toString().padStart(2, '0')}-${date.getUTCDate().toString().padStart(2, '0')}`
    for (const [localDistrictName, localDistrict] of byLocalDistrict.entries()) {
        files[`stolpersteine-berlin-not-on-osm-no-nearby-${localDistrictName.replace(/ /g,'-').toLocaleLowerCase()}.gpx`] = osmElementToGpx(localDistrict.missing.map(convertToNode), generateDescription, {
            name: `Stolpersteine Berlin ${localDistrictName} [${dateString}]`,
            copyright,
        });
    }

    return {
        files,
        text,
        textDe,
    };
}
