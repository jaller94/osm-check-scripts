import { esc } from './helper';

describe('helper', () => {
    describe('esc', () => {
        it('should escape special characters', () => {
            expect(esc('test[123]')).toBe(String.raw`test\[123\]`);
        });
        
        it('should return non-string values as-is', () => {
            expect(esc(123)).toBe(123);
        });
    });
}); 