import { getOpenStreetMapCopyright } from "./helper";
import { osmElementToGpx } from "./helper-geo";
import { OsmElement } from "./types";

export function entranceAccessibilityMissing(elements: OsmElement[]) {
    const arr: OsmElement[] = [];
    for (const element of elements.filter(element => element.tags['railway'] === 'subway_entrance')) {
        if (element.tags['wheelchair'] === undefined) {
            arr.push(element);
        }
    }

    let text = '';
    if (arr.length === 0) {
        text += '✅ Yes, it does.';
    } else {
        text += `⚠️ No, ${arr.length} station entrance${arr.length > 1 ? 's' : ''} have no wheelchair=* tag.`;
    }
    text += '\n\n<!--more-->\n\n';
    text += '## Downloads\n* [As GPX](taiwan-metro-entrances-missing-wheelchair.gpx)\n\n## Issues\n';
    if (arr.length === 0) {
        text += 'Thanks to all contributors! There are no wheelchair=* tags missing.';
    } else {
        for (const element of arr) {
            text += `* [${element.tags['name:en'] ?? element.tags.name ?? 'no name'}](https://osm.org/${element.type}/${element.id})\n`;
        }
    }

    const gpx = osmElementToGpx(arr, e => `${e.tags['name:en'] ?? e.tags.name ?? 'no name'}`, {
        name: 'MRT entrances without wheelchair=*',
        copyright: getOpenStreetMapCopyright(),
    });

    return {
        arr,
        files: {
            'taiwan-metro-entrances-missing-wheelchair.gpx': gpx,
        },
        gpx,
        text,
    }
}
